# JarTools

## Summary

This repository contains a series of scripts that help to manipulate artifacts
in a `.jar` archive. The usual use case scenarios for these tools is when you
have to hack in a class to an existing library that you don't want to recompile
entirely, such as in situations where you don't have source code access) and
only want to splice in a small subset of modifications to the `.jar` archive.

Usually such operations are invasive, and not guaranteed to work. Make sure
you have backups of the originals before using these scripts to manipulate
the artifacts.

## Tools

* `jar-replace` -- replaces a series of classes, from path, or another jar
* `jar-list` -- lists the contents of the jar, by size, to allow for a `diff`