#!/bin/bash

#
# Little script to replace .class files and other artifacts within a .jar archive
#

# Set strict
set -o errexit
set -o nounset

function _usage() {
    local _cmdline

    _cmdline=$(basename "$0")
    echo "${_cmdline}  [-h]"
    echo "${_cmdline}  existing.jar classfile.class [other.class]+"
    echo "${_cmdline}  new.jar existing.jar classfile.class [other.class]+"
    echo " -h : shows this help message"
    exit 0
}

function _error() {
    echo "ERROR: $1"
    if [[ ${2+defined} ]]; then
        echo "  $2"
    fi
    exit 1
}

function _sanity() {
    set +e

    # check that every tool in script is available
    local _r
    local -a _commands

    _commands=("grep" "file" "jar" "mktemp" "pushd" "popd" "readlink" "wc")
    for _c in "${_commands[@]}"; do
        type "$_c" > /dev/null
        if [[ $? -ne 0 ]]; then
            _error "$_c: command not found in path"
        fi
    done

    # GNU readlink isn't platform compatible, need to perform detection to
    # ensure its availability
    _r=$(type "greadlink")
    if [[ $? -eq 0 ]]; then
        eval "CMD_READLINK=greadlink"
    else
        eval "CMD_READLINK=readlink"
    fi

    # sanity check that readlink supports '-f' flag
    "$CMD_READLINK" -f "$0" > /dev/null
    if [[ $? -ne 0 ]]; then
        _error "${CMD_READLINK}: does not appear to support '-f' (resolve absolute path) flag" \
            "For MacOSX, check 'brew install coreutils"
    fi
    set -e
}

function _check_jar() {
    local _r
    local _jar

    _jar="$1"
    if [[ "$_jar" != "" ]]; then
        if [[ ! -f "$_jar" ]]; then
            _error "'$_jar' does not exist"
        fi
        _r=$(file "$_jar")
        set +e
        echo "$_r" | grep -q "Zip archive data"
        if [[ $? -ne 0 ]]; then
            _error "'$_jar' does not appear to be '.jar' archive." "Type reported '$_r'"
        fi
        set -e
    fi
}

function _check_artifacts() {
    local _r
    local _jar
    local -a _classes

    _jar=$1
    _classes=(${@:2})

    set +e
    for _c in "${_classes[@]}"; do
        if [[ "$_jar" != "" ]]; then
            # find all classes in zip archive
            _r=$(jar -tvf "${_jar}" "${_c}" | wc -l)
            if [[ $_r -ne 1 ]]; then
                _error "'$_c' is not found in source '${_jar}'."
            fi
        else
            # find all classes in path given
            if [[ ! -f "$_c" ]]; then
                _error "'$_c' is not found in path"
            fi
        fi
    done
    set -e
}

function _replace() {
    local _tmpdir
    local _oldjar
    local _curjar
    local -a _classes

    if [[ ${2+defined} && ${2##*.} == "jar" ]]; then
        _tmpdir=$(mktemp -d)
        _oldjar=$("$CMD_READLINK" -f "$1")
        _curjar=$("$CMD_READLINK" -f "$2")
        _classes=(${@:3})
    else
        _curjar=$("$CMD_READLINK" -f "$1")
        _classes=(${@:2})
    fi

    _check_jar "$_oldjar"
    _check_jar "$_curjar"

    if [[ "${_oldjar}" == "${_curjar}" ]]; then
        _error "Both jarfiles are the same"
    fi

    if [[ ${#_classes[@]} -eq 0 ]]; then
        _error "No files in arguments"
    fi

    _check_artifacts "$_oldjar" "${_classes[@]}"

    # Extract all class files into transient folder
    if [[ "$_oldjar" != "" ]]; then
        pushd "$_tmpdir"
        for _c in "${_classes[@]}"; do
            jar -xvf "${_oldjar}" "${_c}"
            jar -uvf "${_curjar}" "${_c}"
        done
        popd
        # remove temporary files/directories
        rm -rf "${_tmpdir}"
    else
        jar -uvf "${_curjar}" "${_classes[@]}"
    fi
}

# Globals and main
IS_USAGE=0

function _parse_args() {
    if [[ ${1+defined} && "$1" == "-h" ]]; then
        IS_USAGE=1
    fi
}

function _main() {
    _parse_args "$@"

    if [[ ${IS_USAGE} == 1 ]]; then
        _usage "$@"
    fi

    _sanity
    _replace "$@"
}

_main "$@"
