#!/bin/bash
#
# Extract jar listing from file, and present them in two columns
#   fqcn size
# in sorted order
#
# This omits the date information so that we can allow comparison
# for meld

# set strict
set -o nounset
set -o errexit

if [[ $# -eq 0 ]]; then
    echo "Prints out contents of jar file in sorted order by 'FQCN Size'"
    echo "Usage:"
    echo "    jar-list file.jar"
    exit 1
fi
jar tvf $1 | cut -c '1-7,37-' | sed -Ee 's/^ *([0-9]+) (.*)$/\2 \1/' | sort
